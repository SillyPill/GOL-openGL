//opengl cross platform includes
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#endif

int main_id;
int subMenu_id;
int subMenu_id2;
int size;	   //size of grid //set to 60
int** status;       //0 means cell is dead, 1 means alive
int** statusNew;    //0 means cell is dead, 1 means alive
int speed = 100000; //current speed
int speedup = 1;    //speed levels
int nCount = 0;
int liveCells = 0; //number of live cells on grid
bool clear = false;
bool pauseGame = false;
bool randomize = false;
bool entry = true;
bool page2 = false;
bool page3 = false;
int pageNo = 1;

void cellAlive(int x, int y); //creates a cell at an x,y coord
void drawGrid();	      //draws the lines of the grid
int window_size = 1;

void glider();

void FPS(int val)
{
    glutPostRedisplay();
    glutTimerFunc(300, FPS, 0); // 1sec = 1000, 60fps = 1000/60 = ~17
}

/* intially spawns a few cells to grid */
void drawCellsInitial()
{
    for (int cellX = 0; cellX < size; cellX++) {
	for (int cellY = 0; cellY < size; cellY++) {
	    statusNew[cellX][cellY] = rand() % 2;
	}
    }
}

/* handles the alternating of cells after user clicks */
void clickFunc(int x, int y)
{
    /* printf("%d\n",x); */
    /* printf("%d\n",y); */
    float cell_size;
    /* printf("(%d - 1) / %d\n", size, window_size); */
    cell_size = (float)window_size / ((float)size - 1.0);
    int i = (x / cell_size);
    int j = size - (y / cell_size) - 1;
    /* printf("\n%d,%d", i, j); */

    if (statusNew[i][j] == 1)
	statusNew[i][j] = 0;

    else if (statusNew[i][j] == 0)
	statusNew[i][j] = 1;

    /* printf("\nStatusNew (%d,%d) = %d\n", i,j,statusNew[i][j]); */
}

/* placement of cells on grid by user */
void mouse(int btn, int state, int x, int y)
{
    if (btn == GLUT_LEFT_BUTTON) {
	if (state == GLUT_UP) {
	    /* printf("%s\n", "Alternating cell..."); */
	    clickFunc(x, y);
	}
    }
}

void keyb(unsigned char k, int x, int y)
{

	entry = true;
	if ((int)k == 8) //backspace
	{
		drawCellsInitial();
		entry = false;
		return;
	}
	if ((int)k == '1')
		pageNo = 1;
	if ((int)k == '2')
		pageNo = 2;
	if ((int)k == '3')
		pageNo = 3;
	if ((int)k == '4')
		pageNo = 4;
	if ((int)k == '5')
		pageNo = 5;
	if ((int)k == '6')
		glider();
	if ((int)k == '7')
		pageNo = 7;
	if ((int)k == '8')
		system("xdg-open \'https://www.theguardian.com/science/alexs-adventures-in-numberland/2014/dec/15/the-game-of-life-a-beginners-guide\' &");
	if (k == 'q' || k == 'Q') //menu options
	{
		printf("%s\n", "	Thanks for Playing!\n");
		exit(0);
	}
	if (k == 'c' || k == 'C') {
		clear = !clear;
		printf("%s\n", "	 Grid Cleared.......");
		entry = false;
	}
	if (k == 'p' || k == 'P') {
		pauseGame = !pauseGame;
		printf("%s\n", "	 Pause / Run.......");
		entry = false;
	}
	if (k == 'r' || k == 'R') {
		randomize = !randomize;
		printf("%s\n", "	Refreshing and Randomizing.......");
		entry = false;
	}
	if (k == 's' || k == 'S') {
		if (speedup == 4) { //reset
			speedup = 1;
			printf("%s\n", "  Normal Speed(Default)");
			entry = false;
		} else {
			speedup += 1;
			printf("%s\n", "	 Slowing down.......");
			entry = false;
		}
	}
}

/* draws the grid initially without any cells present */
void drawGrid()
{
    glPointSize(1.0);
    glColor3f(0, 0, 1);
    glBegin(GL_LINES);
    for (int i = 0; i < size; i++) {
	glVertex2f(0, i + 1);
	glVertex2f(size, i + 1);
	glVertex2f(i + 1, 0);
	glVertex2f(i + 1, size);
    }
    glEnd();
}

/* checks left,right,bottom,top. 1 represents alive, 0 represents dead.1 */
int checkNeighbours(int cellX, int cellY)
{
    nCount = 0;

    /* check left */
    if (cellX != 0)
	if (status[cellX - 1][cellY] == 1) {
	    nCount++;
	}

    /* check right */
    if (cellX != (size - 2))
	if (status[cellX + 1][cellY] == 1) {
	    nCount++;
	}

    /* check down */
    if (cellY != 0)
	if (status[cellX][cellY - 1] == 1) {
	    nCount++;
	}

    /* check up */
    if (cellY != (size - 2))
	if (status[cellX][cellY + 1] == 1) {
	    nCount++;
	}

    if (cellX != 0 && cellY != (size - 2))
	if (status[cellX - 1][cellY + 1] == 1) {
	    nCount++;
	} //diag
    if (cellX != (size - 2) && cellY != (size - 2))
	if (status[cellX + 1][cellY + 1] == 1) {
	    nCount++;
	} //diag

    if (cellX != 0 && cellY != 0)
	if (status[cellX - 1][cellY - 1] == 1) {
	    nCount++;
	} //diag

    if (cellX != (size - 2) && cellY != 0)
	if (status[cellX + 1][cellY - 1] == 1) {
	    nCount++;
	} //diag

    return nCount;
}

/* creates a new green cell */
void cellAlive(int x, int y)
{
    glBegin(GL_POLYGON);
    glColor3f(1, 0.7, 0);
    glVertex2f(x, y);
    glVertex2f(x + 1, y);
    glVertex2f(x + 1, y + 1);
    glVertex2f(x, y + 1);
    glEnd();
}

/* clears the grid of all live cells */
void clearBoard()
{
    for (int cellX = 0; cellX < size; cellX++) {
	for (int cellY = 0; cellY < size; cellY++) {
	    status[cellX][cellY] = 0;
	    statusNew[cellX][cellY] = 0;
	}
    }
}

/* finds cells that are alive */
void drawCells()
{
    for (int i = 0; i < size; i++) {
	for (int j = 0; j < size; j++) {
	    if ((status[i][j]) == 1) {
		/* draw on that cell */
		cellAlive(i, j);
	    }
	}
    }
}

/* rules of the game are used to set values to coordinates. 1 is alive, 0 is dead */
void rules()
{
    usleep(speed);

    for (int i = 0; i < size; i++) { //copies new to old based on rules of the game
	for (int j = 0; j < size; j++) {
	    status[i][j] = statusNew[i][j];
	}
    }

    drawCells(); //redraws board with all new cells

    for (int cellX = 0; cellX < size - 1; cellX++) {
	for (int cellY = 0; cellY < size - 1; cellY++) {
	    int n = checkNeighbours(cellX, cellY);
	    if (n == 3 && (status[cellX][cellY] == 0)) { //new cell spawns
		statusNew[cellX][cellY] = 1;
		liveCells++;
	    } else if ((n == 2 && (status[cellX][cellY] == 1)) || (n == 3 && (status[cellX][cellY] == 1))) { //lives on to next generation
		statusNew[cellX][cellY] = 1;
	    } else if (n < 2 && (status[cellX][cellY] == 1)) { //cell dies by underpopulation
		statusNew[cellX][cellY] = 0;
		liveCells--;
	    } else if (n > 3 && (status[cellX][cellY] == 1)) { //cell dies by overcrowding
		statusNew[cellX][cellY] = 0;
		liveCells--;
	    }
	}
    }
}

inline void printhead(char* c)
{
    while (*c)
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c++);
}
inline void printSLine(char* c)
{
    while (*c)
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *c++);
}

void GOL_rules(void)
{
    glColor3f(1, 0, 0);
    glRasterPos2i(2, (10 * size) / 12);
    printSLine("1. Any live cell with fewer than two live neighbors dies, ");

    glRasterPos2i(2, (9 * size) / 12);
    printSLine("   caused by under population.");

    glColor3f(1, 1, 0);
    glRasterPos2i(2, (8 * size) / 12);

    printSLine("2. Any live cell with two or three live neighbors lives on to the next generation");

    glColor3f(1, 0, 0);
    glRasterPos2i(2, (7 * size) / 12);

    printSLine("3. Any live cell with more than three live neighbors dies, as if by overpopulation");

    glColor3f(0, 1, 0);
    glRasterPos2i(2, (6 * size) / 12);

    printSLine("4. Any dead cell with exactly three live neighbors becomes a live cell,");
    glRasterPos2i(2, (5 * size) / 12);

    printSLine("   as if by reproduction");

    glColor3f(1, 1, 1);

    glRasterPos2i(size / 2, size / 10);
    printSLine("Press 4 to continue..");
}

void aboutGliders()
{

    glColor3f(0, 1, 1);
    glRasterPos2i(2, (10 * size) / 12);
    printSLine("The glider is a pattern that travels across the board in Conway's Game of Life");
    glRasterPos2i(2, (9 * size) / 12);
    printSLine("Gliders are the smallest spaceships, and they travel diagonally at a speed of c/4.");
    glRasterPos2i(2, (8 * size) / 12);
    printSLine("The glider is often produced from randomly generated starting configurations");
    glRasterPos2i(2, (7 * size) / 12);
    printSLine("After seeing it animated, Conway has remarked the glider looks more like an ant");
    glRasterPos2i(2, (6 * size) / 12);
    printSLine("...be computed within Conway's Game of Life");
    /* glRasterPos2i(2, (5*size)/12 ); */
    /* printSLine("The Cellular automata evolves according to the following rules."); */
    glColor3f(1, 1, 1);
    glRasterPos2i(size / 8, size / 10);
    printSLine("Press 6 to see a glider in action");
}

void open_link()
{

    glColor3f(0, 1, 1);
    glRasterPos2i(2, (10 * size) / 12);
    printSLine("There are many more interesting patterns to be observed in GOL");
    glRasterPos2i(2, (9 * size) / 12);
    printSLine("such as the eater, spaceships, still lives.");
    glRasterPos2i(2, (8 * size) / 12);
    printSLine("We can also produce the basic logic gates using these rules");
    glRasterPos2i(2, (7 * size) / 12);
    /* printSLine("After seeing it animated, Conway has remarked the glider looks more like an ant"); */
    /* glRasterPos2i(2, (6 * size) / 12); */
    /* printSLine("...be computed within Conway's Game of Life"); */
    /* glRasterPos2i(2, (5*size)/12 ); */
    /* printSLine("The Cellular automata evolves according to the following rules."); */
    glColor3f(1, 1, 1);
    glRasterPos2i(size / 8, size / 10);
    printSLine("Press 8 to visit a website with more information...");
}

void glider()
{
    clearBoard();
    statusNew[0][(size - 5) + 0] = 1;
    statusNew[1][(size - 5) + 0] = 1;
    statusNew[2][(size - 5) + 0] = 1;
    statusNew[2][(size - 5) + 1] = 1;
    statusNew[1][(size - 5) + 2] = 1;
    entry = false;
}

void aboutGOL2(void)
{
    glColor3f(0, 1, 0);
    glRasterPos2i(2, (10 * size) / 12);
    printSLine("The Game of Life was devised by the British mathematicial John Conway");
    glRasterPos2i(2, (9 * size) / 12);
    printSLine("From a theoretical point of view, it is interesting because...");
    glRasterPos2i(2, (8 * size) / 12);
    printSLine("...it has the power of a \"universal Turing machine\"");
    glRasterPos2i(2, (7 * size) / 12);
    printSLine("That is, anything that can be computed algorithmically can");
    glRasterPos2i(2, (6 * size) / 12);
    printSLine("...be computed within Conway's Game of Life");
    /* glRasterPos2i(2, (5*size)/12 ); */
    /* printSLine("The Cellular automata evolves according to the following rules."); */
    glColor3f(1, 1, 1);
    glRasterPos2i(size / 8, size / 10);
    glRasterPos2i(2, (4 * size) / 12);
    printSLine("Press <backspace> anytime to start with a randomized matrix..");
    glRasterPos2i(2, (3 * size) / 12);
    printSLine("Press 5 to learn about Gliders");
}

void aboutGOL(void)
{
    glColor3f(0, 1, 0);
    glRasterPos2i(2, (10 * size) / 12);
    printSLine("The \"game\" is a \"zero-player\" cellular automata.");
    glRasterPos2i(2, (9 * size) / 12);
    printSLine("Its evolution is determined by its initial state.");
    glColor3f(0, 1, 1);
    glRasterPos2i(2, (8 * size) / 12);
    printSLine("One interacts with the Game of Life by creating an initial configuration...");
    glRasterPos2i(2, (7 * size) / 12);
    printSLine("...and observing how it evolves.");
    glRasterPos2i(2, (6 * size) / 12);
    printSLine("or, for advanced \"players\" by creating patterns with particular properties");
    glColor3f(0, 1, 0);
    glRasterPos2i(2, (5 * size) / 12);
    printSLine("The Cellular automata evolves according to the following rules.");
    glRasterPos2i(size / 2, size / 10);
    printSLine("Press 3 to continue..");
}

void coverpage(void)
{
    glColor3f(0, 1, 0);
    glRasterPos2i(2, (7 * size) / 8);
    printhead("Computer Graphics Labratory Mini-Project");
    glColor3f(1, 1, 0);
    glRasterPos2i(2, (3 * size) / 4);
    printhead("Conway's Game of Life :\n Universal Cellular Automata");
    glColor3f(0, 1, 1);
    glRasterPos2i(2, size / 2);
    printhead("By, Adithya Kumar N S (1KS15CS006)");
    glColor3f(0, 1, 0);
    glRasterPos2i(size / 2, size / 10);
    printSLine("Press 2 to continue..");
}

void display(void)
{
    glClearColor(0, 0, 0, 0);
    glMatrixMode(GL_MODELVIEW);
    /* glLoadIdentity(); */
    glClear(GL_COLOR_BUFFER_BIT);
    if (entry == true) {
	switch (pageNo) {
	case 1:
	    coverpage();
	    break;
	case 2:
	    aboutGOL();
	    break;
	case 3:
	    GOL_rules();
	    break;
	case 4:
	    aboutGOL2();
	    break;
	case 5:
	    aboutGliders();
	    break;
	case 7:
	    open_link();
	    break;
	}
    } else {

	if (pauseGame == false) {
	    drawGrid(); //draws grid lines only
	    rules();    //rules of Conway
	    drawGrid();
	}
	if (clear == true) {
	    clearBoard();
	}
	if (randomize == true) {
	    drawCellsInitial();
	    randomize = !randomize;
	    clear = false;
	}
	if (speedup == 1) {
	    speed = 100000;
	} //speed control(normal)
	else if (speedup == 1) {
	    speed = 100000;
	} //slower
	else if (speedup == 2) {
	    speed = 400000;
	} //""
	else if (speedup == 3) {
	    speed = 800000;
	} //""
	else if (speedup == 4) {
	    speed = 1000000;
	} //slower
    }
    glFlush();
}

/* main function - program entry point */
int main(int argc, char** argv)
{
    printf("\n%s", "Please enter the size of the World (40-300): ");
    scanf("%d", &size);
    size++;
    printf("\n%s\n", "\tWelcome!");
    printf("\n%s\n", "Press c to clear the grid");
    printf("%s\n", "Press r to refresh and randomize the grid");
    printf("%s\n", "Press p to pause/run the program");
    printf("%s\n", "Press s for speed control: Normal(Default)->Slower->Slower->Slower->Normal");
    printf("%s\n", "Click on the grid to alternate cells");
    printf("%s\n", "Press q to quit the program");
    status = (int**)malloc(size * sizeof(int*));    //dynamic allocation of the status array.
    statusNew = (int**)malloc(size * sizeof(int*)); //dynamic allocation of the status array.
    for (int i = 0; i < size; i++) {
	status[i] = (int*)malloc(size * sizeof(int));
	statusNew[i] = (int*)malloc(size * sizeof(int));
    }
    drawCellsInitial();    //set down a few live cells in the beginning
    glutInit(&argc, argv); //starts up GLUT
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
    window_size = (700 > (size * 4)) ? 700 : (size * 4);
    glutInitWindowSize(window_size, window_size);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Conway's Game of Life"); //creates the window
    gluOrtho2D(0, size - 1, 0, size - 1);      //for size * size grid. Index starts at 0.
    glutDisplayFunc(display);		       //registers "display" as the display callback function
    glutKeyboardFunc(keyb);
    glutMouseFunc(mouse);
    glutTimerFunc(0, FPS, 0);
    glutMainLoop(); //starts the event loop
    return (0);     //return may not be necessary on all compilers
}
